SUBDIRS = ilo toki-pona-parser

.PHONY: all

all:
	for dir in $(SUBDIRS); do \
		$(MAKE) -C $$dir; \
	done
