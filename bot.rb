require 'discordrb'
require 'oj'

# Don't actually replace this line in this file with your token!
# Replace it inside the .token file
replace_text = "/// REPLACE THIS LINE WITH YOUR TOKEN ///"
invalid_text = "\nERROR: \n\tThe token file is invalid.\n\tPlease acquire a token by creating " +
  "a bot account at\n\t" +
  "https://discordapp.com/login?redirect_to=/developers/applications/me\n\t" +
  "and paste it inside the .token file in this directory."

file = File.open(".token", "a+")
token = if file.eof? then false else file.readline.strip end

if(!token)
  file.puts replace_text
  abort(invalid_text)
elsif(token == replace_text)
  abort(invalid_text)
end

class Profile
  attr_accessor :tp_word_count, :extinct_word_count, :word_count, :jans, :frequency, :dnt

  def initialize
    @tp_word_count = 0
    @extinct_word_count = 0
    @word_count = 0
    @jans = 0
    @frequency = Hash.new { |hash, key| hash[key] = 0 }
    @dnt = false
  end

  def to_s(name)
    if @word_count != 0
      "**nanpa nimi pi toki #{name}**\n" +
      "`toki pona:       #{@tp_word_count}`\n" +
      "`toki majuna:     #{@extinct_word_count}`\n" +
      "`toki ali:        #{@word_count}`\n" +
      "`toki jans:       #{@jans}`\n" +
      "`toki pona / ali: #{((@tp_word_count.to_f / @word_count) * 10000.0).round / 100.0}%`"
    else
      "**sina toki ala!**"
    end
  end

end

@profiles = Hash.new { |hash, key| hash[key] = Profile.new }

def load_profiles
  if(!File.zero?('profiles.json'))
    file = File.read('profiles.json')
    @profiles = Hash.new { |hash, key| hash[key] = Profile.new }
    puts Oj.load(file)
    @profiles = @profiles.merge(Oj.load(file))
    @profiles.each_value do |profile|
      frequency = Hash.new { |hash, key| hash[key] = 0 }
      profile.frequency = frequency.merge(profile.frequency)
    end
  end
end

def save_profiles
  File.open('profiles.json', 'w') do |f|
    f.write(Oj.dump(@profiles))
  end
end

load_profiles

@tp_dict = Hash.new(false)
@extinct_dict = Hash.new(false)
@dnt = Hash.new(false)
@dnt[162019334397362176] = true

File.readlines('tp_dict.txt').each { |line| @tp_dict[line.strip] = true }
File.readlines('extinct_dict.txt').each { |line| @extinct_dict[line.strip] = true }

def char_replacer word, subs
  word.chars.map { |c| subs.key?(c) ? subs[c] : c }.join
end

def replace_nonalpha_chars str
  str.gsub(/(\W|\d)/, ' ')
end

def tokenize_string str
  str.gsub(/\s+/m, ' ').gsub(/^\s+|\s+$/m, '').split(" ")
end

reg = /\b([klmnps][aeiou]|[aeiou]|[jt][aeou]|w[aei])(n[klps][aeiou]|[klmnps][aeiou]|n?[jt][aeou]|n?w[aei])*n?\b/i

bot = Discordrb::Commands::CommandBot.new(
token: token,
client_id: 351960756851441665,
prefix: 'toki!')

bot.message(start_with: not!("toki!")) do |event|
  if !event.from_bot?
    profile = @profiles[event.author.id]
    if !@dnt[event.author.id]
      words = replace_nonalpha_chars(event.content)
      words = tokenize_string(words)
      words.each do |word|
        if(word.downcase == "jans")
          profile.jans += 1
        elsif(@tp_dict[word.downcase])
          profile.tp_word_count += 1
          profile.frequency[word.downcase] += 1
        elsif(@extinct_dict[word.downcase])
          profile.extinct_word_count += 1
          profile.frequency[word.downcase] += 1
        end
        if(word.downcase != "a")
          profile.word_count += 1
        end
      end
    end
  end
  save_profiles
  nil
end

bot.command(:mi, description: "View your user profile.") do |event|
  @profiles[event.author.id].to_s(event.author.mention)
end

bot.command(:janali, description: "View all user's aggregate data.") do |event|
  ali = Profile.new
  @profiles.each_value do |profile|
    ali.tp_word_count += profile.tp_word_count
    ali.extinct_word_count += profile.extinct_word_count
    ali.word_count += profile.word_count
    ali.jans += profile.jans
  end
  ali.to_s("pi jan ali")
end

bot.command(:ilo, description: "Runs some toki code.") do |event|
  /(toki!ilo\s*)(`)*(\s)*(?<p>([^`]|\s)*)(`)*/m =~ event.content
  File.write('prog.toki', p)
  output = IO.popen('./ilo/interp.byte prog.toki')
  event.respond("```\n#{(output.readlines * '')}```")
  nil
end

bot.command(:nimi, description: "Verifies whether given words are phonologically valid in toki pona.") do |event, *words|
  pona = []
  ike = []
  /(toki!nimi\s*)(`)*(\s)*(?<p>([^`]|\s)*)(`)*/m =~ event.content
  if words.empty?
    event.respond("`/#{reg}/`")
  else
    words.each do |word|
      word = char_replacer word, { '`' => '', '.' => '', ',' => '', '?' => '', '!' => '', '~' => '' }
      if(word == "jans")
        event.respond("nimi jans li **pona sewi mute a** tawa toki pona")
      elsif reg.match?(word)
        pona << word
      else
        ike << word
      end
    end
    if !pona.empty?
      event.respond("nimi \"#{pona.join(', ')}\" li **pona** tawa toki pona")
    end
    if !ike.empty?
      event.respond("nimi \"#{ike.join(', ')}\" li **ike** tawa toki pona")
    end
  end
  nil
end

bot.command(:nanpa, description: "Converts numerals to toki pona numbers and vice-versa.") do |event, number|
  if number =~ /\A[-+]?[0-9]+\z/
    number = number.to_i
    if(number == 0)
      result = "ala"
    else
      result = ""
      l = lambda { |n, w|
        while number >= n do
          result += w
          number -= n
        end
      }
      l.call 100, "ali "
      l.call 20, "mute "
      l.call 5, "luka "
      l.call 2, "tu "
      l.call 1, "wan "
    end
  else
    result = "TODO"
  end
  result
end

bot.command(:pu, description: "Verifies whether some given toki pona is pu.") do |event|
  /(toki!pu\s*)(`)*(\s)*(?<p>([^`]|\s)*)(`)*/m =~ event.content
  File.write('pu.txt', p)
  output = IO.popen('./toki-pona-parser/pu.byte pu.txt')
  event.respond("```\n#{(output.readlines * '')}```")
  nil
end

bot.run :async

bot.game=("say toki!help")

loop do
  sleep(30)
end
